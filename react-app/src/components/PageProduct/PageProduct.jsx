import './PageProduct.css'

import Colors  from '../Colors/Colors';

const PageProduct = () => {
    return (

    <div> 
        <header className="header" id="top">
            <div className="container header-container">
                <div className="header-logo">
                  <div className="base"><img className="jpeg" src="images/favicon.ico"/></div>
                  <span className="color-orange">Мой</span>Маркет
                 </div>
                <div className="icons">
                    <div className="icon">
                        <img className="icon__img" src="images/cart.svg" alt="Корзина"/>
                        <span className="icon__indicator" id="cart-indicator"></span>
                    </div>
                </div>
            </div>
        </header>
      
        <div className="container">
            <div className ="breadcrumbs">
                  <a className="breadcrumb" href="#">Электроника</a> {'>'} 
                  <a className="breadcrumb" href="#">Cмартфоны и гаджеты</a>{ '>'} 
                  <a className="breadcrumb" href="#">Мобильные телефоны</a> {'>'} 
                  <a className="breadcrumb" href="#">Apple</a> {'>'} 
            </div>
            <div className="product-name">
               <h2>Смартфон Apple iPhone 13</h2>
            </div>
   
            <div className="product-image">
               <img src="./images/image-1.webp" alt="Смартфон Apple iPhone 13" height="200"/>
               <img src="./images/image-2.webp" alt="Смартфон Apple iPhone 13" height="200"/>
               <img src="./images/image-3.webp" alt="Смартфон Apple iPhone 13" height="200"/>
               <img src="./images/image-4.webp" alt="Смартфон Apple iPhone 13" height="200"/>
               <img src="./images/image-5.webp" alt="Смартфон Apple iPhone 13" height="200"/>
            </div>
            
            <div className="product-parts">    
            
               <section className="product-info">
                  <Colors />
                  <div className="product-info-subsection">
                     <h3 className="product-info__subtitle">Цвет товара: синий</h3>
                        <div className="product-info__gallery">
                           <div className="product-info__gallery-item">
                              <img src="./images/color-1.webp" alt="Красный смартфон Apple iPhone 13" height="60"/>
                           </div>
                           <div className="product-info__gallery-item">
                              <img src="./images/color-2.webp" alt="Зеленый смартфон Apple Iphone 13" height="60"/>
                           </div>
                           <div className="product-info__gallery-item">
                              <img src="./images/color-3.webp" alt="Розовый смартфон Apple Iphone 13" height="60"/>
                           </div>
                           <div className="product-info__gallery-item product-info__gallery-item--active">
                              <img src="./images/color-4.webp" alt="Синий смартфон Apple Iphone 13" height= "60"/>
                           </div>
                           <div className="product-info__gallery-item">
                              <img src="./images/color-5.webp" alt="Голубой смартфон Apple Iphone 13" height="60"/>
                           </div>
                           <div className="product-info__gallery-item">
                              <img src="./images/color-6.webp" alt="Черный смартфон Apple iphone 13" height="60"/>
                           </div>
                        </div>
                  </div>


                  <div className="product-info-subsection">   
                     <h3 className="product-info-subtitle">Конфигурация памяти: 128 Гб</h3>
                        <div className="product-info-subtitle__btn">
                           <button className="product-info__btn product-info__btn--active">128 ГБ</button>    
                           <button className="product-info__btn">256 ГБ</button>
                           <button className="product-info__btn">512 ГБ</button>
                        </div>
                  </div>
                  
                  <div className="product-info-subsection">   
                  <h3>Характеристики товара</h3>
                        <ul className="product-info__features">
                           <li className="product-info__features-item">Экран :<b> 6.1</b></li>
                           <li className="product-info__features-item">Встроенная память: <b>128 Гб</b></li>
                           <li className="product-info__features-item">Операционная система: <b>iOS 15</b></li>
                           <li className="product-info__features-item">Беспроводные интерфейсы: <b>NFS, Bluetooth,Wi-Fi</b></li>
                           <li className="product-info__features-item">Процессор: <a target="_blank" rel="noreferrer" href="https://ru.wikipedia.org/wiki/Apple_A15">Apple A15 Bionic</a></li>
                           <li className="product-info__features-item">Вес: 173 г</li> 
                        </ul>
                  </div> 

                  <div className="product-info-subsection">
                  <h3 className="product-info-subtitle">Описание</h3>
                     <p>Наша самая совершенная система двух камер.<br/>
                        Особый взгляд на прочность дисплея.<br/>
                        Чип, с которым всё супербыстро.<br/>
                        Аккумулятор держится заметно дольше.<br/>
                        <i>iPhone 13 - сильный мира всего.</i>
                     </p>
                     <p>Мы разработали совершенно новую схему расположения и  развернули объективы на 45 градусов.
                        Благодаря этому внутри корпуса поместилась нашалучшая система двух камер с увеличенной матрицей 
                        широкоугольной камеры.Кроме того, мы освободили место для системы оптической стабилизации 
                        изображения сдвигом матрицы. И повысили скорость работы матрицы насверхширокоугольной камере.
                        </p>
                     <p>Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. Новая 
                        широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео.
                        Новая оптическая стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом 
                        положении.
                     </p>
                     <p>Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения
                        резкости. Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки,
                        создавая красивый эффект размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести
                        фокус на другого человека или объект, который появился в кадре. Теперь ваши видео будут смотреться как 
                        настоящее кино
                     </p>
                  </div>   

                  <div className="product-info-subsection">
                  <h3 className="product-table-subtitle">Сравнение моделей</h3>
                     <table className="product-table">
                        <thead className="product-table__info">
                           <tr className="product-table__detail">
                              <th className="product-table__detail-item">Модель</th>
                                 <th>Вес</th>
                                 <th>Высота</th>
                                 <th>Ширина</th>
                                 <th>Толщина</th>
                                 <th>Чип</th>
                                 <th>Объем памяти</th>
                                 <th>Аккумулятор</th>
                                 </tr>
                        </thead>
                           <tbody>
                           <tr className="product-table__detail-information">
                                 <td>Iphone11</td>
                                 <td>194 грамма</td>
                                 <td>150,9 мм</td>
                                 <td>75,7 мм</td>
                                 <td>8,3 мм</td>
                                 <td>A13 Bionic chip</td>
                                 <td>до 128 Гб</td>
                                 <td>до 17 часов</td>
                              </tr>
                                 <tr className="product-table__detail-information">
                                 <td>Iphone12</td>
                                 <td>164 грамма</td>
                                 <td>146,7 мм</td>
                                 <td>71,5 мм</td>
                                 <td>7,4 мм</td>
                                 <td>A14 Bionic chip</td>
                                 <td>до 256 Гб</td>
                                 <td>до 19 часов</td>
                              </tr>
                              <tr className="product-table__detail-information">
                                 <td>Iphone13</td>
                                 <td>174 грамма</td>
                                 <td>146,7 мм</td>
                                 <td>71,5 мм</td>
                                 <td>7,65 мм</td>
                                 <td>A15 Bionic chip</td>
                                 <td>до 512 Гб</td>
                                 <td>до 19 часов</td>
                                 </tr>
                           </tbody>
                     </table>   
                  </div>
                  <div className="product-review"/>
                     <div className="product-review__title">
                        <div className="product-review__title-one"> 
                           <h3 className="product-review-title">Отзывы</h3>
                        <div className="product-review-amount">425</div>
                     </div>
                  
                     </div>
                        <div className="product-reviews">
                           <div className="product-review__detail">
                              <div className="product-review__detail-photo">
                                 <img className="img-rounded" src="images/review-1.jpeg" alt="Марк Г." height="140"/>
                              </div>
                                 <div className="product-review__detail-content">
                                    <div className="product-review__detail-name">
                                    <p><b>Марк Г.</b></p>
                                    </div>
                                       <div className="star">
                                          <img src="images/star-5.png" alt="5 звезд" height="30"/>
                                       </div>
                                          <div className="product-review__detail-information">
                                             <p><b>Опыт использования :</b> менее месяца</p>
                                             <p><b>Достоинства:</b><br/>
                                                   это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко
                                                   и красиво. довольно шустрое устройство. камера весьма неплохая, Q ширик тоже на высоте.
                                             </p>
                                             <p><b>Недостатки:</b><br/>
                                                   к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если 
                                                   нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное
                                             </p>
                                          </div>
                                    </div>   
                           </div>
                        
         
                           <div className="line"></div>
         
                           <div className="product-review__detail"> 
                              <div className="product-review__detail-photo">
                                 <img className="img-rounded" src="images/review-2.jpeg" alt="Валерий Коваленко" height="140"/> 
                              </div>
                                 <div className="product-review__detail-content">
                                    <div className="product-review__detail-name">
                                       <p><b>Валерий Коваленко</b></p>
                                    </div>
                                       <div className="star">
                                          <img src="images/star-4.png" alt="4 звезды" height="30"/>
                                       </div>
                                          <div className="product-review__detail-information">
                                             <p><b>Опыт использования :</b> менее месяца</p>
                                             <p><b>Достоинства:</b><br/>
                                                   OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго
                                             </p>
                                             <p><b>Недостатки:</b><br/>
                                                   Плохая ремонтопригодность
                                             </p> 
                                          </div>
                                 </div>  
                           </div>
                  </div>
                  
                        <form className="form" id="review-form"> 
                           <h4 className="product-form-subtitle">Добавить свой отзыв</h4>
                              <div className="form__row">
                                 <div className="form__column">
                                    <input type="text" placeholder="имя и фамилия" name="review_name"/> 
                                    <div className="form-review__error" id="review-form-name-error"></div>
                                 </div>
                                 <div className="form__column">
                                    <input type="text" placeholder="оценка" name="review_score"/>
                                    <div className="form-review__error" id="review-form-score-error"></div>
                                 </div>
                              </div>
                              <div className="form__row">
                                 <textarea id ="review" placeholder="Текст отзыва"></textarea>
                              </div>
                              <div className="form-footer">
                                 <button className="button-review">Добавить отзыв</button> 
                              </div>
                        </form>
               </section>
               
               <section className="product-sidebar">  
                  <section className="product-sidebar-important">
                     <div className="product_price">
                        <div className="product_price-header">
                           <div className="old">
                              <div className="cost1">
                                 75 990 ₽
                              </div> 
                              <div className="discount">
                                 -8%  
                              </div> 
                           </div>
                           <div className="like">
                              <img className="svg" src="/images/like.svg"/>
                           </div>
                        </div>
                        <div className="cost2">
                           67 990 ₽
                        </div>                           
                     </div>
                     <div className="product-delivery">
                        <p>Самовывоз в четверг, 1 сентября-<b>бесплатно</b></p>
                        <p>Курьером в четверг, 1 сентября-<b>бесплатно</b></p> 
                     </div>    

                     
                     <div className="buttons">
                        <button className="cart-button" id="cart-button">
                           <img className="card-button_icon" src="/images/cart.btn.svg"/>
                           <div className="cart-button_text" id="cart-button-text">Добавить в корзину</div>
                        </button>
                     </div>
                            
                  </section>             
                     <p className="hoverublicity">Реклама</p>
                     <iframe src="ads.html" title="Заголовок"></iframe>
                     <iframe src="ads.html" title="Заголовок"></iframe>
               </section>
            </div>
        </div>
                    
         <div className ="footer">
            <div className="container footer-сontainer">
               <div className="footer-content">
                  <p>© OOO Мой Маркет 2018-2022</p>  
                  <p>Для уточнения информации звоните по номеру <a className="contact" href="tel:79000000000">+7 900 000 0000</a>
                     а предложения по сотрудничеству отправляйте на почту <a className="contact">partner@mymarket.com</a>
                    </p>
               </div>
               <div className="inscription">
                  <p>наверх
                  </p>
               </div>
            </div>
         </div>
    </div>       
    );
};
export default PageProduct