import { useState } from "react";
import './Colors.css';

import ColorButton from "./ColorButton";

const Colors = () => {

  const colorsImages = [
    {
      id: 1,
      name: 'Красный',
      src: 'images/color-1.webp',
      alt: 'Красный смартфон Apple iPhone 13',
    },
    {
      id: 2,
      name: 'Зеленый',
      src: 'images/color-2.webp',
      alt: 'Зеленый смартфон Apple iPhone 13',
    },
    {
      id: 3,
      name: 'Розовый',
      src: 'images/color-3.webp',
      alt: 'Розовый смартфон Apple iPhone 13',
    },
    {
      id: 4,
      name: 'Синий',
      src: 'images/color-4.webp',
      alt: 'Синий смартфон Apple iPhone 13',
    },
    {
      id: 5,
      name: 'Белый',
      src: 'images/color-5.webp',
      alt: 'Белый смартфон Apple iPhone 13',
    },
    {
      id: 6,
      name: 'Черный',
      src: 'images/color-6.webp',
      alt: 'Черный смартфон Apple iPhone 13',
    },
  ];

  const [activeColorImageId, setActiveColorImageId] = useState(1);

  const changeActiveColor = (id) => {
    setActiveColorImageId(id);
  };
  
  return (
    <div className="product-info__subsection">
      <h4 className="product-info__subtitle">Цвет товара: { colorsImages[activeColorImageId - 1].name }</h4>

      <div className="product-info__gallery">
        {colorsImages.map((item) => {
          return <ColorButton 
                    key={item.id}
                    id={item.id}
                    name={item.name}
                    src={item.src}
                    alt={item.alt}
                    active={activeColorImageId === item.id}
                    changeActiveColor={changeActiveColor} 
                  />
        })}
      </div>
    </div>
  );
}

export default Colors;
