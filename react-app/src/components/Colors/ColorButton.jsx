import './ColorButton.css'

const ColorButton = (props) => {
  const { id, name, src, alt, active, changeActiveColor } = props;

  const handleClick = () => {
    console.log(id, name);
    changeActiveColor(id);
  }

  const itemClassName = active 
                          ? 'product-info__gallery-item product-info__gallery-item--active'
                          : 'product-info__gallery-item';

  return (
    <div className={itemClassName} onClick={handleClick}>
      <img src={src} alt={alt} height="100" />
    </div>
  );
}

export default ColorButton;
