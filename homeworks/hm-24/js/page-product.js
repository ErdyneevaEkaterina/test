// Инициализация переменных
const reviewForm = document.getElementById('review-form');
console.log('reviewForm', reviewForm)

const reviewFormName = document.getElementById('review-form-name');
const reviewFormNameError = document.getElementById('review-form-name-error');

const reviewFormScore = document.getElementById('review-form-score');
const reviewFormScoreError = document.getElementById('review-form-score-error');

let nameErrorStr = '';
let scoreErrorStr = '';

// Инициализация полей, исходя из содержимого localStorage
if (localStorage.getItem('reviewName')) {
  reviewFormName.value = localStorage.getItem('reviewName')
}

if (localStorage.getItem('reviewScore')) {
  reviewFormScore.value = localStorage.getItem('reviewScore')
}

// Обработка изменений полей, запись в localStorage
reviewFormName.addEventListener('input', (e) => {
  localStorage.setItem('reviewName', e.target.value)
  console.log(e.target.value)
})

reviewFormScore.addEventListener('input', (e) => {
  localStorage.setItem('reviewScore', e.target.value)
})


// Обработка отправки формы
reviewForm.addEventListener('submit', (e) => {
  e.preventDefault();
  console.log('Form was submitted with js');
  console.log('#review-form-name value:', reviewFormName.value);

  // Формирование строки Ошибки имени
  if (reviewFormName.value === '') {
    nameErrorStr = 'Вы забыли указать имя и фамилию';
  } else if (reviewFormName.value.length < 3) {
    nameErrorStr = 'Имя не может быть короче двух символов';
  } else {
    nameErrorStr = '';
  }
  console.log('nameErrorStr:', nameErrorStr );

  // Формирование строки Ошибки оценки
  if (reviewFormScore.value === '') {
    scoreErrorStr = 'Поле не заполнено: Оценка должна быть от 1 до 5';
  } else if (!reviewFormScore.value.match(/^\d+$/)) {
    scoreErrorStr = 'В поле введены буквы: Оценка должна быть от 1 до 5';
  } else if (!reviewFormScore.value.match(/^[1-5]$/)) {
    scoreErrorStr = 'Вы ввели цифры больше 5 или меньше 1: Оценка должна быть от 1 до 5';
  } else {
    scoreErrorStr = '';
  }
  console.log('scoreErrorStr:', scoreErrorStr);

  // Добавление/удаление стилей и контента для Ошибки имени
  if (nameErrorStr) {
    reviewFormNameError.innerHTML = nameErrorStr;
    reviewFormNameError.classList.add('form-review__error-active');
  } else {
    reviewFormNameError.innerHTML = '';
    reviewFormNameError.classList.remove('form-review__error-active');
  }

  // Добавление/удаление стилей и контента для Ошибки оценки
  if (scoreErrorStr) {
    reviewFormScoreError.innerHTML = scoreErrorStr
    reviewFormScoreError.classList.add('form-review__error-active');
  } else {
    reviewFormScoreError.innerHTML = '';
    reviewFormScoreError.classList.remove('form-review__error-active');
  }
  if (!nameErrorStr && !scoreErrorStr) {
    reviewFormName.value = '';
    reviewFormScore.value = '';

    // localStorage.setItem('reviewName', '');
    // localStorage.setItem('reviewScore', '');

    localStorage.removeItem('reviewName');
    localStorage.removeItem('reviewScore');
  }
})
