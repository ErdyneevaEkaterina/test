// Инициализация числа товаров в корзине, включая проверку в localStorage
let goodsCountInCart = localStorage.getItem('goodsCountInCart') ? Number(localStorage.getItem('goodsCountInCart')) : 0;
console.log('goodsCountInCart - Начальное значение', goodsCountInCart)

// Инициализация переменных для html-элементов
const cartIndicator = document.querySelector('#cart-indicator');

const cartButton = document.querySelector('#cart-button');
const cartButtonText = document.querySelector('#cart-button-text');

// Сценарий 1: товар уже был добавлен, его нужно удалить - При первой загрузке, из-за непустого localStorage
if (goodsCountInCart === 1) {
  cartIndicator.innerHTML = goodsCountInCart;
  cartIndicator.classList.toggle('icon__indicator-active');

  cartButtonText.innerHTML = 'Товар уже в корзине';
  cartButton.classList.add('cart-button-added');
}

// Обработка клика по кнопке Добавить в корзину/Удалить из корзины
cartButton.addEventListener('click', () => {
  console.log('goodsCountInCart - Значение при клике', goodsCountInCart)

  // Сценарий 1: товар уже был добавлен, его нужно удалить
  if (goodsCountInCart === 1) {
    console.log('Сценарий 1: товар уже был добавлен, его нужно удалить');

    goodsCountInCart = 0;
    localStorage.removeItem('goodsCountInCart');

    cartIndicator.innerHTML = '';
    cartIndicator.classList.toggle('icon__indicator-active');

    cartButton.classList.toggle('cart-button-added');
    cartButtonText.innerHTML = 'Добавить в корзину';
  } 
  
  // Сценарий 2: товар еще не был добавлен, его нужно добавить
  else {
    console.log('Сценарий 2: товар еще не был добавлен, его нужно добавить');

    goodsCountInCart = 1;
    localStorage.setItem('goodsCountInCart', goodsCountInCart);

    cartIndicator.innerHTML = goodsCountInCart;
    cartIndicator.classList.toggle('icon__indicator-active');

    cartButton.classList.toggle('cart-button-added');
    cartButtonText.innerHTML = 'Товар уже в корзине';
  }

});
