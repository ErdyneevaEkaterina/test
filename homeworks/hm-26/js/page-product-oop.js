// Родительский класс
class Form {
    makeAlertAfterSubmitForm = () => {
      alert('Форма успешно отправлена!');
    }
  }
  
  // Класс валидации формы отзывов
  class AddReviewForm extends Form {
    // Поля класса
    reviewForm = null;
  
    reviewFormName = null;
    reviewFormNameError = null;
  
    reviewFormScore = null;
    reviewFormScoreError = null;
  
    nameErrorStr = '';
    scoreErrorStr = '';
  
    // Конструктор класса
    constructor(formQuerySelector) {
      super();
  
      // Создание ссылок на html элементы
      this.reviewForm = document.querySelector(formQuerySelector);
      this.reviewFormName = document.querySelector(`${formQuerySelector} input[name="review_name"]`);
      this.reviewFormNameError = document.querySelector(`${formQuerySelector} input[name="review_name"] + div`);
      this.reviewFormScore = document.querySelector(`${formQuerySelector} input[name="review_score"]`);
      this.reviewFormScoreError = document.querySelector(`${formQuerySelector} input[name="review_score"] + div`);
  
      // Консоли для демонстрации
      console.group('Variables for HTML elements');
      console.log('this.reviewForm', this.reviewForm);
      console.log('this.reviewFormName', this.reviewFormName);
      console.log('this.reviewFormNameError', this.reviewFormNameError);
      console.log('this.reviewFormScore', this.reviewFormScore);
      console.log('this.reviewFormScoreError', this.reviewFormScoreError);
      console.groupEnd();
  
      // Проверка на наличие значение в localStorage
      if (localStorage.getItem('reviewName')) {
        this.reviewFormName.value = localStorage.getItem('reviewName')
      }
      if (localStorage.getItem('reviewScore')) {
        this.reviewFormScore.value = localStorage.getItem('reviewScore')
      }
  
      // Запись в localStorage
      this.reviewFormName.addEventListener('input', (e) => {
        localStorage.setItem('reviewName', e.target.value)
        this.reviewFormNameError.classList.remove('form-review__error-active');
      })
      this.reviewFormScore.addEventListener('input', (e) => {
        localStorage.setItem('reviewScore', e.target.value)
      })
  
      // Скрытие ошибки при повторном вводе
      this.reviewFormName.addEventListener('focus', (e) => {
        this.reviewFormNameError.classList.remove('form-review__error-active');
        this.reviewFormName.classList.remove('form-review__error-active');
      })
      this.reviewFormScore.addEventListener('focus', (e) => {
        this.reviewFormScoreError.classList.remove('form-review__error-active');
        this.reviewFormScore.classList.remove('form-review__error-active');
      })
    }
  
    // Метод валидации поля имени
    validateName = (nameValue) => {
      if (nameValue === '') {
        this.nameErrorStr = 'Вы забыли указать имя и фамилию';
      } else if (nameValue.length < 2) {
        this.nameErrorStr = 'Имя не может быть короче двух символов';
      } else {
        this.nameErrorStr = '';
      }
    };
  
    // Метод валидации поля оценки
    validateScore = (scoreValue) => {
      if (scoreValue === '') {
        this.scoreErrorStr = 'Поле не заполнено: Оценка должна быть от 1 до 5';
      } else if (!scoreValue.match(/^\d+$/)) {
        this.scoreErrorStr = 'В поле введены буквы: Оценка должна быть от 1 до 5';
      } else if (!scoreValue.match(/^[1-5]$/)) {
        this.scoreErrorStr = 'Вы ввели цифры больше 5 или меньше 1: Оценка должна быть от 1 до 5';
      } else {
        this.scoreErrorStr = '';
      }
    };
  
    submitForm = () => {
      this.reviewForm.addEventListener('submit', (e) => {
        e.preventDefault();
  
        // Валидация полей
        this.validateName(this.reviewFormName.value);
        this.validateScore(this.reviewFormScore.value);
  
        // Консоли для демонстрации
        console.group('Submit form');
        console.log('this.reviewFormName.value', this.reviewFormName.value);
        console.log('this.nameErrorStr', this.nameErrorStr);
        console.log('this.reviewFormScore.value', this.reviewFormScore.value);
        console.log('this.scoreErrorStr', this.scoreErrorStr);
        console.groupEnd();
  
        // Оформление поля имени
        if (this.nameErrorStr) {
          this.reviewFormNameError.innerHTML = this.nameErrorStr;
          this.reviewFormNameError.classList.add('form-review__error-active');
          this.reviewFormName.classList.add('form-review__error-active');
        } else {
          this.reviewFormNameError.innerHTML = '';
          this.reviewFormNameError.classList.remove('form-review__error-active');
          this.reviewFormName.classList.remove('form-review__error-active');
        }
  
        // Оформление поля оценки
        if (!this.nameErrorStr && this.scoreErrorStr) {
          this.reviewFormScoreError.innerHTML = this.scoreErrorStr;
          this.reviewFormScoreError.classList.add('form-review__error-active');
          this.reviewFormScore.classList.add('form-review__error-active');
        } else {
          this.reviewFormScoreError.innerHTML = '';
          this.reviewFormScoreError.classList.remove('form-review__error-active');
          this.reviewFormScore.classList.remove('form-review__error-active');
        }
  
        // Успешная валидация, сброс localStorage
        if (!this.nameErrorStr && !this.scoreErrorStr) {
          this.reviewFormName.value = '';
          this.reviewFormScore.value = '';
  
          localStorage.removeItem('reviewName');
          localStorage.removeItem('reviewScore');
  
          this.makeAlertAfterSubmitForm();
        }
      });
    };
  }
  
  // Создание экземпляра класса
  const addReviewForm = new AddReviewForm('#review-form');
  addReviewForm.submitForm();
  