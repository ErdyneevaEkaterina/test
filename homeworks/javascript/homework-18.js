// Упражнение 1
let a='100px';
let b='323px';
let result = parseInt ("100px")+parseInt ("100px")
console.log(result);


// Упражнение 2
let max = Math.max(10, -45, 102, 36, 12, 0, -1)
console.log(max);

// Упражнение 3
let d = 0.111; // Округлить до 1
// Решение 
let d1 = Math.ceil(0.111)
console.log(d1);

let e = 45.333333; // Округлить до 45.3
// Решение 
let e1 = e.toFixed(2);
console.log(e1);

let f = 3; // Возвести в степень 5(должно быть 243)
// Решение
let fPow5 = f ** 5;
console.log(fPow5);

// ===!== - строгие сравнения


let g = 400000000000000; // Записать в сокращеном виде
// Решение
console.log('g:', g)
const gShortform = 4e14;
console.log('g short form:', gShortform)
console.log('g toExponential:',g.toExponential(0))

let h ='1'!= 1;
console.log ('h:',h);
h='1' !== 1;
console.log('h strict comparison:', h);
// ===!== - строгие сравнения

console.groupEnd();