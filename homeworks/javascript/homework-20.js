'use strict'

// Упражнение 1

for (let i = 0; i <= 20; i++) {
    if (i % 2 === 0) console.log(i);
    //if (i % 2 === 0 && i !== 0) console.log(1);
}

// for (let i = 0; i <=20; i = i + 2) {
//  console.log (i);
// }

// Упражнение 2

let sum = 0;
let error = false;
for (let i = 0; i < 3; i++) {
    let numStr = prompt('Введите число:');
    let num = Number(numStr);

    if (!numStr || isNaN(num)) {
        error = true;
        console.log('Ошибка, вы ввели не число');
        break;   
    }
    console.log (`Вы ввели число ${num}`)
    sum +=num;
}
if (!error) console.log('sum', sum);


// Упражнение 3
const monthsArr = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']

// Объявление функции
const getNameOfMonth = (monthNumber) => {
    return monthsArr[monthNumber];
};

console.log('getNameofMonth(0)', getNameOfMonth(0))

for (let i = 0; i < monthsArr.length; i++) {
    // Вызов функции
    if (getNameOfMonth(i)!== 'Октябрь') console.log(getNameOfMonth(i));
};

// Перебирающие методы массива
// monthsArr.forEach((item) => {
// if (item !=='Октябрь') console.log(item);
// });

