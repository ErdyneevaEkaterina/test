'use strict'

// Exercise 1
let timerNum = prompt('Введите стартовое число для обратного отсчета');
if (!Number(timerNum)) alert('Вы ввели не число');

let timerId = setInterval(function() {
  if (timerNum === 0) {
		clearInterval(timerId);
    console.log('Время вышло!');
    return;
	}

	console.log(`Осталось ${timerNum}`);
  timerNum--;
}, 1000);


// Exercise 2, way 2
fetch('https://reqres.in/api/users')
  .then((usersResponse) => {
    console.log('usersResponse', usersResponse);
    return usersResponse.json();
  })
  .then((usersObj) => {
    console.log('usersObj', usersObj);
    return usersObj.data;
  })
  .then((users) => {
    console.log('users', users);

    if (users) {
      console.log(`Получили пользователей: ${users.length}`);

      users.forEach((user) => {
        console.log(`— ${user.first_name} ${user.last_name} (${user.email})`);
      });
    }
  })
  .catch((error) => {
    console.error(error);
  })