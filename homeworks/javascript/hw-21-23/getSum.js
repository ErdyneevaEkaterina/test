/**
*Get sum of array of numbers, non-numeric elements are skipped
*@param {Array}
*@returns {Number}
*/
export const getSum = (arr) => {
    let sum = 0;

    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === 'number'){
            sum += arr[i];
        }
    }

    return sum;
}