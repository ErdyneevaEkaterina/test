'use strict'

// Импорты всегда вверху файла
import { product, review } from './data.js';

// Exercise 1
const isEmpty = (obj) => {
    return Object.keys(obj).length === 0;
}

const objEx1 = {};
console.log(isEmpty(objEx1));

objEx1.title = 'Some article';
console.log(isEmpty(objEx1));

// Exercise 2
console.group('Product');
console.log('product:', product);
console.log('product name:', product);
console.log('product features, memory:', product.features.memory);
console.log('product features, interfaces:', product.features.interfaces);
console.groupEnd();

console.group('Review');
console.log('review:', review);
console.log('review rating:', review.rating);
console.log('review author:', review.author);
console.groupEnd();

// Exercise 3
const raiseSalaryWayTwo = (salaries,percent) => {
    let sumOfNewSalaries = 0;

    for (const key in salaries) {
        salaries [key] *= 1 +(percent / 100);
        sumOfNewSalaries  += salaries[key];
    }

    console.log ('sumOfNewSalaries', sumOfNewSalaries);

    return salaries;
};

let salariesWayTwo  = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};
console.log('salariesWayTwo before function raiseSalaryWaytwocall:', salariesWayTwo);

salariesWayTwo =raiseSalaryWayTwo(salariesWayTwo, 5)
console.log('salariesWayTwo after function raiseSalaryWaytwocall', salariesWayTwo);
