export const product = {
    name: 'iPhone 13',
    features: {
        color: 'синий',
        memory: '128 ГБ',
        screen: '6,1',
        os: 'IOS 15',
        interfaces: 'Wi-fi', 
        core: 'Apple A15 Bionic',
        weight: '173 г',
    },
    description: 'Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому внутри корпуса поместилась нашалучшая система двух камер с увеличенной матрицей широкоугольной камеры.Кроме того, мы освободили место для системы оптической стабилизации изображения сдвигом матрицы. И повысили скорость работы матрицы насверхширокоугольной камере.',
    price: 67990,
    priceold: 75990,
    currency: '₽',
    discount: 8,
    images: ["/images/images-1.webp", "/images/images-2.webp","/images/images-3.webp","/images/images-4.webp","/images/images-5.webp"],
    colors: ["Синий","Красный","Зеленый","Белый"],
    memory: [128, 256, 512],
    deliveryMethods: [{
        name:"Самовывоз",
        date: "1 сентября, четверг",
        cost: 67990,   
    }, {
        name: "Курьером",
        date: "1 сентября, четверг",
        cost: 67990,
    }],
};
    
// Сведения об авторе
export const review = {
    author: {
        name:'Марк Г.',
        photo:'images/star-5.png',
    },
    rating: 5,
    experieceofusage: 'менее месяца',
    text: 'some text',
    advantages: 'это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно',
    disadvantages: 'к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь',
};