'Use strict'

// Импорты всегда вверху файла
import { product } from './data.js';
import { getSum } from './getSum.js';

// Exercise 1

const arr1 = [1, 2, 10, 5];
console.log(arr1, getSum(arr1));

const arr2 = ["a",{},3, 3, -2];
console.log(arr2, getSum(arr2));

// Exercise 2
console.log(product.images);
console.log(product.colors);
console.log(product.memory);
console.log(product.deliveryMethods);

// Exercise 3

const addToCart = (id) => {
    if (!cart.includes(id)) {
      cart.push(id);
    }
  };
  
  const removeFromCart = (id) => {
    for (const index in cart) {
    if (cart[index] === id) {
        cart.splice(index, 1);
        break;
      }
    }
  };
  
  const cart = [4884];
  console.log(cart);
  
  addToCart(3456);
  addToCart(3456);
  addToCart(3458);
  console.log(cart);
  
  removeFromCart(3456);
  console.log(cart);
  

  
